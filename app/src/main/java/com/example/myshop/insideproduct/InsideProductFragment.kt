package com.example.myshop.insideproduct

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.example.myshop.R
import com.example.myshop.api.ApiInterface
import com.example.myshop.api.retrofitmaker
import com.example.myshop.helper.Constants
import com.example.myshop.models.Product
import com.example.myshop.models.SingleProduct
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import kotlinx.android.synthetic.main.costum_product.view.*
import kotlinx.android.synthetic.main.fragment_inside_product.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.create
import retrofit2.http.Path

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"


/**
 * A simple [Fragment] subclass.
 * Use the [InsideProductFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class InsideProductFragment : Fragment() , OnMapReadyCallback{


   var idd=0


    private lateinit var mMap: GoogleMap


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            idd=it.getInt(ARG_PARAM1)

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_inside_product, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


        val api=retrofitmaker.builderetrofit().create(ApiInterface::class.java)
       api.getproduct(idd).enqueue(object :Callback<SingleProduct>{
           override fun onFailure(call: Call<SingleProduct>, t: Throwable) {
               Toast.makeText(requireContext(), "gfgdgsdg", Toast.LENGTH_SHORT).show()
           }

           override fun onResponse(call: Call<SingleProduct>, response: Response<SingleProduct>) {
               if (response.isSuccessful){

                   val product = response.body()!!.product
                   Log.e("test",Gson().toJson(response.body()!!))
                   tv_name_inside.setText(product.name)
                   tv_gheymat_inside.setText(product.price.toString())
                   tv_tozih_inside.setText(product.description)
                   Glide.with(requireContext())
                       .load(Constants.PRODUCT_PIC_ADDRESS+
                       product.pic
                       ).into(iv_pic_inside)

                   val mahsuo = LatLng(product.lat.toDouble(), product.lang.toDouble())
                   mMap.addMarker(MarkerOptions().position(mahsuo))
                   mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mahsuo,12f))

               }
           }

       })











    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment InsideProductFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(id:Int) =
            InsideProductFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_PARAM1, id)

                }
            }
    }

    override fun onMapReady(p0: GoogleMap) {
        mMap=p0

    }
}