package com.example.myshop.fragment_new

import android.R.attr.country
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.example.myshop.R
import com.example.myshop.api.ApiInterface
import com.example.myshop.api.retrofitmaker
import com.example.myshop.bede
import com.example.myshop.easyToast
import com.example.myshop.insideproduct.InsideProductFragment
import com.example.myshop.models.Category
import com.example.myshop.models.CategoryRespons
import com.example.myshop.models.SingleProduct
import com.example.myshop.namayesh
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.Task
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.android.synthetic.main.activity_maps.*
import kotlinx.android.synthetic.main.fragment_new.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File


class NewFragment : Fragment(), OnMapReadyCallback {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null



    private lateinit var mMap: GoogleMap



    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private lateinit var task : Task<LocationSettingsResponse>

    private lateinit var locationCallback: LocationCallback
    // 2
    private lateinit var locationRequest: LocationRequest
    private var locationUpdateState = false

    var lastLocation: Location? =null

    var pic : File? = null

    val categories = ArrayList<Category>()

    lateinit var spinner_adapter : ArrayAdapter<String>




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map_new_product) as SupportMapFragment
        mapFragment.getMapAsync(this)
        btn_find_location.setOnClickListener({

            requestLocationWithPermission();


        })
        val api = retrofitmaker.builderetrofit().create(ApiInterface::class.java)
        api.getcategoris().enqueue(object : Callback<CategoryRespons> {
            override fun onResponse(
                call: Call<CategoryRespons>,
                response: Response<CategoryRespons>
            ) {
                pb_new_cat.namayesh(false)
                gp_new.namayesh(true)
                if (response.isSuccessful) {
                    categories.addAll(response.body()!!.Categoris)
                    val cat_name = ArrayList<String>();
                    categories.forEach {
                        cat_name.add(it.name)
                    }

                    spinner_adapter=ArrayAdapter(requireContext(),android.R.layout.simple_spinner_item,cat_name)
                    spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)


                    spinner_cat.adapter=spinner_adapter

                }

            }

            override fun onFailure(call: Call<CategoryRespons>, t: Throwable) {
                pb_new_cat.namayesh(false)

            }

        })




        initLocation()

        btn_make_prouct.setOnClickListener {
            val name = et_name_product.bede()
            val description = et_description_product.bede()
            val price = et_price_product.bede()
            val latlang = mMap.cameraPosition.target


            val cat_id = categories.get(spinner_cat.selectedItemPosition).cat_id

            if (pic==null) {
                "لطفا تصویر رو انتخاب نمایید".easyToast(requireContext())
                return@setOnClickListener
            }

            val api = retrofitmaker.builderetrofit().create(ApiInterface::class.java)


            val requestBody = RequestBody.create(MediaType.parse("Image/*"), pic!!)
            val part_file = MultipartBody.Part.createFormData("pic", pic!!.getName(), requestBody)




            pb_new.namayesh(true)

            api.makeProduct(
                part_file,
                name,
                price.toInt(),
                description,
                latlang.latitude,
                latlang.longitude,
                cat_id
            ).enqueue(object : Callback<SingleProduct> {
                override fun onResponse(
                    call: Call<SingleProduct>,
                    response: Response<SingleProduct>
                ) {

                    pb_new.namayesh(false)

                    if (response.isSuccessful) {
                        "محصول ثبت شد".easyToast(requireContext())
                        val fragment =
                            InsideProductFragment.newInstance(response.body()!!.product.product_id)
                        childFragmentManager.beginTransaction().replace(
                            R.id.container_new,
                            fragment
                        ).addToBackStack(null).commit()


                    } else {
                        val error = response.errorBody()!!.string()
                        Log.e("myerror", error)
                    }
                }

                override fun onFailure(call: Call<SingleProduct>, t: Throwable) {
                    pb_new.namayesh(false)

                    "خطا".easyToast(requireContext())
                }

            });




        }

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
                super.onLocationResult(p0)
                lastLocation = p0.lastLocation
                mMap.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(
                            lastLocation!!.latitude,
                            lastLocation!!.longitude
                        ), 14f
                    )
                )
                fusedLocationClient.removeLocationUpdates(locationCallback)
            }
        }


        iv_gallery.setOnClickListener{
            Dexter.withContext(requireContext())
                .withPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(object : PermissionListener {
                    override fun onPermissionGranted(p0: PermissionGrantedResponse?) {
                        val intent = Intent()
                        intent.type = "image/*"
                        intent.action = Intent.ACTION_GET_CONTENT
                        startActivityForResult(
                            Intent.createChooser(intent, "انتخاب عکس"),
                            545
                        )
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        p0: PermissionRequest?,
                        p1: PermissionToken?
                    ) {
                        Toast.makeText(requireContext(), "qablam rad shode", Toast.LENGTH_LONG)
                            .show()
                        p1?.continuePermissionRequest()
                    }

                    override fun onPermissionDenied(p0: PermissionDeniedResponse?) {
                        Toast.makeText(requireContext(), "dade nashod", Toast.LENGTH_LONG).show()
                    }

                })
                .check();
        }


    }





    private fun initLocation() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        locationRequest = LocationRequest()
        // 2
        locationRequest.interval = 10000
        // 3
        locationRequest.fastestInterval = 5000
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)

        // 4
        val client = LocationServices.getSettingsClient(requireActivity())
        task = client.checkLocationSettings(builder.build())

    }


    private  fun requestLocationWithPermission() {
        Dexter.withContext(requireActivity())
            .withPermission(android.Manifest.permission.ACCESS_FINE_LOCATION)
            .withListener(object : PermissionListener {
                @SuppressLint("MissingPermission")
                override fun onPermissionGranted(p0: PermissionGrantedResponse?) {
                    mMap.isMyLocationEnabled = true
                    mMap.getUiSettings().setMyLocationButtonEnabled(false);
                    createLocationRequest()
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: PermissionRequest?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }

                override fun onPermissionDenied(p0: PermissionDeniedResponse?) {
                }

            }).check()
    }




    @SuppressLint("MissingPermission")
    private fun createLocationRequest() {
        task.addOnSuccessListener {
            locationUpdateState = true
            fusedLocationClient.lastLocation.addOnSuccessListener {
                it?.let {
                    mMap.animateCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            LatLng(
                                it.latitude,
                                it.longitude
                            ), 14f
                        )
                    )
                    return@let
                }
            }
            fusedLocationClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                null /* Looper */
            )

        }
        task.addOnFailureListener { e ->
            // 6
            if (e is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
//                    e.startResolutionForResult(requireActivity(),
//                        REQUEST_CHECK_SETTINGS)
                    startIntentSenderForResult(
                        e.getResolution().getIntentSender(),
                        REQUEST_CHECK_SETTINGS,
                        null,
                        0,
                        0,
                        0,
                        null
                    );


                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }
    }


    @RequiresApi(Build.VERSION_CODES.KITKAT)
    @SuppressLint("MissingPermission")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == Activity.RESULT_OK) {
                locationUpdateState = true
                fusedLocationClient.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    null /* Looper */
                )
            }
        }else if(requestCode==545) {
            val mypic = data?.data.toString();
            iv_gallery.setImageURI(Uri.parse(mypic))

            val address = getPathFromURI(requireContext(), data!!.data!!)


            pic= File(address!!)
        }




    }



    @RequiresApi(Build.VERSION_CODES.KITKAT)
    private fun getPathFromURI(context: Context, uri: Uri): String {
        var realPath = String()
        uri.path?.let { path ->

            val databaseUri: Uri
            val selection: String?
            val selectionArgs: Array<String>?
            if (path.contains("/document/image:")) { // files selected from "Documents"
                databaseUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                selection = "_id=?"
                selectionArgs = arrayOf(DocumentsContract.getDocumentId(uri).split(":")[1])
            } else { // files selected from all other sources, especially on Samsung devices
                databaseUri = uri
                selection = null
                selectionArgs = null
            }
            try {
                val column = "_data"
                val projection = arrayOf(column)
                val cursor = context.contentResolver.query(
                    databaseUri,
                    projection,
                    selection,
                    selectionArgs,
                    null
                )
                cursor?.let {
                    if (it.moveToFirst()) {
                        val columnIndex = cursor.getColumnIndexOrThrow(column)
                        realPath = cursor.getString(columnIndex)
                    }
                    cursor.close()
                }
            } catch (e: Exception) {
                println(e)
            }
        }
        return realPath
    }



    companion object {


        private const val REQUEST_CHECK_SETTINGS = 2

//
//        @JvmStatic
//        fun newInstance(param1: String, param2: String) =
//            NewFragment().apply {
//                arguments = Bundle().apply {
//                    putString(ARG_PARAM1, param1)
//                    putString(ARG_PARAM2, param2)
//                }
//            }
    }

    override fun onMapReady(p0: GoogleMap) {
       mMap=p0;
        val location_karaj = LatLng(35.8269154, 50.8899934)
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location_karaj, 12f))
    }
}