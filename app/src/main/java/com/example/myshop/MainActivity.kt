package com.example.myshop

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import androidx.fragment.app.Fragment
import com.example.myshop.fragment_category.CategoryFragment
import com.example.myshop.fragment_home.HomeFragment
import com.example.myshop.fragment_new.NewFragment
import com.example.myshop.fragment_profile.ProfileFragment
import com.example.myshop.fragment_dasteh.SaerchFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iv_hamber.setOnClickListener({
            drawer.openDrawer(Gravity.RIGHT)

        })
        openfregment(HomeFragment())
        bnv.setOnNavigationItemReselectedListener({
            if (it.itemId==R.id.bnv_home){
                openfregment(HomeFragment())
            }else if(it.itemId==R.id.bnv_category){
                openfregment(CategoryFragment())
            }else if (it.itemId==R.id.bnv_new){
                openfregment(NewFragment())
            }else if (it.itemId==R.id.bnv_profile){
                openfregment(ProfileFragment())
            }else{
                openfregment(SaerchFragment())
            }

        })

    }
    fun openfregment(fragment: Fragment){
        val transaction=supportFragmentManager.beginTransaction()
        transaction.replace(R.id.cl_container,fragment)
        transaction.commit()


    }


}
