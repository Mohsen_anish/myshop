package com.example.myshop

import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.AppCompatEditText


fun AppCompatEditText.bede() : String {
    return text.toString()
}

fun String.easyToast(context : Context) {
    Toast.makeText(context,this,Toast.LENGTH_LONG).show()
}

fun View.namayesh(show: Boolean) {

    if (show) {
        this.visibility=View.VISIBLE
    }else {
        this.visibility=View.GONE
    }
}