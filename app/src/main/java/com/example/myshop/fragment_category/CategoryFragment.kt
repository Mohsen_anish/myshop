package com.example.myshop.fragment_category

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.myshop.R
import com.example.myshop.api.ApiInterface
import com.example.myshop.api.retrofitmaker
import com.example.myshop.models.Category
import com.example.myshop.models.CategoryRespons
import kotlinx.android.synthetic.main.fragment_category.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.create

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [CategoryFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CategoryFragment : Fragment() {
    val categoris=ArrayList<Category>()
    lateinit var adapter:CategoryAdapter
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        adapter= CategoryAdapter(requireContext(),categoris)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        pb_category.visibility=View.VISIBLE
        rv_category.adapter=adapter
        val api=retrofitmaker.builderetrofit().create(ApiInterface::class.java)
        api.getcategoris().enqueue(object :Callback<CategoryRespons>{
            override fun onFailure(call: Call<CategoryRespons>, t: Throwable) {

            }

            override fun onResponse(
                call: Call<CategoryRespons>,
                response: Response<CategoryRespons>
            ) {
                pb_category.visibility=View.GONE
               if (response.isSuccessful){
                   categoris.addAll(response.body()!!.Categoris)
                   adapter.notifyDataSetChanged()


               }
            }

        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_category, container, false)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment CategoryFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            CategoryFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}