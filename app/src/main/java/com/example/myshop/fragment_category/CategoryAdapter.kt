package com.example.myshop.fragment_category

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.myshop.R
import com.example.myshop.entty.Category
import com.example.myshop.helper.Constants
import kotlinx.android.synthetic.main.custom_category.view.*

class CategoryAdapter(val context: Context, val categoris:ArrayList<com.example.myshop.models.Category>) :RecyclerView.Adapter<CategoryAdapter.Holder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.custom_category,parent,false)
        return Holder(view)

    }

    override fun getItemCount(): Int {
        return categoris.size

    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.itmView.tv_name_category.setText(categoris.get(position).name)
        Glide.with(context).load(Constants.CATEGORY_PIC_ADDRESS + categoris.get(position).pic).into(holder.itemView.iv_category)




    }





    class Holder(val itmView: View):RecyclerView.ViewHolder(itmView){


    }


}