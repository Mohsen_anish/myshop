package com.example.myshop.fragment_profile

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.myshop.R
import com.example.myshop.api.ApiInterface
import com.example.myshop.api.retrofitmaker
import com.example.myshop.fragment_register.RigisterFragment
import com.example.myshop.models.LoginRequest
import com.example.myshop.models.RegisterResponse
import kotlinx.android.synthetic.main.fragment_profile.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.http.Body

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class ProfileFragment : Fragment() {

    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        btn_signup.setOnClickListener({
            childFragmentManager.beginTransaction().replace(R.id.container_register,RigisterFragment()).commit()
        })
        btn_login.setOnClickListener({
            val email=et_email_login.text.toString()
            val password=et_password_login.text.toString()
            if (email.isEmpty()){
                Toast.makeText(requireContext(), "ایمیل را وارد کنید", Toast.LENGTH_SHORT).show()
            }else if (password.isEmpty()){
                Toast.makeText(requireContext(), "ارمز عبور را وارد کنید", Toast.LENGTH_SHORT).show()

            }else{
                pb_login.visibility=View.VISIBLE
                val api=retrofitmaker.builderetrofit().create(ApiInterface::class.java)
                val loginRequest=LoginRequest(email,password)
                api.login(loginRequest).enqueue(object :Callback<RegisterResponse>{
                    override fun onFailure(call: Call<RegisterResponse>, t: Throwable) {
                        pb_login.visibility=View.GONE
                        Toast.makeText(requireContext(), "عدم دسترسی به اینترنت", Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(
                        call: Call<RegisterResponse>,
                        response: Response<RegisterResponse>
                    ) {
                        pb_login.visibility=View.GONE
                        if (response.isSuccessful){
                            pb_login.visibility=View.GONE
                            Toast.makeText(requireContext(), "خوش امدید"+response.body()!!.user.name, Toast.LENGTH_SHORT).show()
                        }else{
                            Toast.makeText(requireContext(), "عدم ارتباظ با سرور", Toast.LENGTH_SHORT).show()
                        }

                    }

                })
            }
        })

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ProfileFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}