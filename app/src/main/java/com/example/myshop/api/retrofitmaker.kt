package com.example.myshop.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class retrofitmaker {

companion object{
    val retrofit:Retrofit?=null
    fun builderetrofit():Retrofit {
        if (retrofit!= null) {
            return retrofit
        } else{
          return Retrofit.Builder().baseUrl("https://poushka.com/shop/public/api/")
              .addConverterFactory(GsonConverterFactory.create())
              .build()
              }

        }
    }
}
