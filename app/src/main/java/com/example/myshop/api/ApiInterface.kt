package com.example.myshop.api

import com.example.myshop.models.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface ApiInterface {

@POST("register")
fun rigistr(@Body   registerRequest: RegisterRequest)  :  Call<RegisterResponse>
@POST("login")
fun login(@Body   loginRequest: LoginRequest)    :  Call<RegisterResponse>
@GET("getProducts")
fun getproducts(): Call<ProductsResponse>
    @GET("getCategories")
    fun getcategoris():Call<CategoryRespons>
    @GET("product/{product_id}")
  fun getproduct(@Path("product_id") product_id: Int):Call<SingleProduct>


  @Multipart
  @POST("makeProduct/{cat_id}")
  fun makeProduct(@Part pic : MultipartBody.Part ,@Part("name") name: String,
                  @Part("price") price : Int,@Part("description") descrioion : String ,
                  @Part("lat") lat : Double , @Part("lang") lang : Double,
                  @Path("cat_id") cat_id : Int
                  ) :  Call<SingleProduct>

//    getProductByCatId/{cat_id}



}