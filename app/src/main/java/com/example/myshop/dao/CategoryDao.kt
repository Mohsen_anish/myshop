package com.example.myshop.dao

import android.icu.util.ULocale
import androidx.room.Dao
import androidx.room.Query
import com.example.myshop.entty.Category

@Dao
interface CategoryDao {
   fun insertcategory(category: Category):Long
    @Query("select * from category ")
    fun getcategoris():List<Category>
}