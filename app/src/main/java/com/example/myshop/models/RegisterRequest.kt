package com.example.myshop.models

import com.google.gson.annotations.SerializedName

data class RegisterRequest(@SerializedName("namee")  val name:String, val email:String, val password:String)