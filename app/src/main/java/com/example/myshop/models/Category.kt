package com.example.myshop.models

data class Category(val cat_id:Int, val name:String, val pic:String)