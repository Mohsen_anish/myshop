package com.example.myshop.models

import com.google.gson.annotations.SerializedName

data class RegisterResponse(val massage:String,val stutus:Int,@SerializedName("data") val user:User)