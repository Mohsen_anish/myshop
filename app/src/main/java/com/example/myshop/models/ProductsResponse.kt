package com.example.myshop.models

import com.google.gson.annotations.SerializedName

data class ProductsResponse(@SerializedName("data") val products:ArrayList<Product>, val message:String, val status:Int)
