package com.example.myshop.models

import com.google.gson.annotations.SerializedName

data class SingleProduct(@SerializedName("data") val product:Product)