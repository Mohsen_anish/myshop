package com.example.myshop.fragment_register

import android.os.Bundle
import android.speech.RecognitionService
import android.util.LogPrinter
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.myshop.R
import com.example.myshop.api.ApiInterface
import com.example.myshop.api.retrofitmaker
import com.example.myshop.models.RegisterRequest
import com.example.myshop.models.RegisterResponse
import kotlinx.android.synthetic.main.fragment_rigister.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import kotlin.math.log

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [RigisterFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RigisterFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)

        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_rigister, container, false)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btn_rigister.setOnClickListener({
            val name=et_name_rigister.text.toString()
            val email=et_email_rigister.text.toString()
            val password=et_password_rigister.text.toString()
            val re_password=et_repassword_rigister.text.toString()
            if (name.isEmpty()){
                Toast.makeText(requireContext(), "نام را وارد نمایید", Toast.LENGTH_SHORT).show()
            }else if (email.isEmpty()){
                Toast.makeText(requireContext(), "ایمیل را وارد نمایید", Toast.LENGTH_SHORT).show()
            }else if (password.isEmpty()){
                Toast.makeText(requireContext(), "رمز عبور را وارد نمایید", Toast.LENGTH_SHORT).show()
            }else if (!password.equals(re_password)){
                Toast.makeText(requireContext(), "رمز عبور و تکرار برابر نمیباشد", Toast.LENGTH_SHORT).show()

            }else{
                val apiinterface=retrofitmaker.builderetrofit().create(ApiInterface::class.java)
                val registerrequest=RegisterRequest(name,email,password)
                apiinterface.rigistr(registerrequest).enqueue(object:Callback<RegisterResponse>{
                    override fun onFailure(call: Call<RegisterResponse>, t: Throwable) {

                        Toast.makeText(requireContext(), "عدم دسترسی به اینترنت", Toast.LENGTH_SHORT).show()

                        AlertDialog.Builder(requireContext()).setTitle("test")
                            .setMessage(t.localizedMessage).create().show()

                    }

                    override fun onResponse(
                        call: Call<RegisterResponse>,
                        response: Response<RegisterResponse>

                    ) {
                        if (response.isSuccessful){
                            Toast.makeText(requireContext(), "ممنون از ثبت نام شما"+ response.body()!!.user.name, Toast.LENGTH_SHORT).show()

                        }else{

                        }

                    }
                } )

            }

        })

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment RigisterFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            RigisterFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}