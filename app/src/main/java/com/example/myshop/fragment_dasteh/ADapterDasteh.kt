package com.example.myshop.fragment_dasteh

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myshop.R
import com.example.myshop.models.Dasteh
import kotlinx.android.synthetic.main.custom_dasteh.view.*
import kotlinx.android.synthetic.main.fragment_dasteh.view.*

class ADapterDasteh(val context: Context,val dasteha:ArrayList<Dasteh>,val cliclistener: Cliclistener):RecyclerView.Adapter<ADapterDasteh.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view=LayoutInflater.from(context).inflate(R.layout.custom_dasteh,parent,false)
        return Holder(view)
    }

    override fun getItemCount(): Int {
        return dasteha.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
       holder.itmView.tv_name_dasteh.text=dasteha[position].onvan
        val myadapter=ZirdastehAdapter(context,dasteha.get(position).zirdasteh)
        holder.itmView.rv_zirdasteh.adapter=myadapter
        if (dasteha[position].open){
            holder.itmView.rv_zirdasteh.visibility=View.VISIBLE
        }else{
            holder.itmView.rv_zirdasteh.visibility=View.GONE
        }
        holder.itmView.setOnClickListener({
            cliclistener.addclicked(holder.adapterPosition)
        })
    }

    class Holder(val itmView: View):RecyclerView.ViewHolder(itmView){

    }
    interface Cliclistener{
        fun addclicked(position: Int)
    }
}