package com.example.myshop.fragment_dasteh

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myshop.R
import kotlinx.android.synthetic.main.custom_zirdasteh.view.*

class ZirdastehAdapter(val context: Context,val zirdasteha:ArrayList<String>):RecyclerView.Adapter<ZirdastehAdapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
       val view=LayoutInflater.from(context).inflate(R.layout.custom_zirdasteh,parent,false)
        return Holder(view)
    }

    override fun getItemCount(): Int {
        return zirdasteha.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.itmView.tv_name_zirdasteh.text=zirdasteha[position]
    }
    class Holder(val itmView: View) :RecyclerView.ViewHolder(itmView){

    }


}