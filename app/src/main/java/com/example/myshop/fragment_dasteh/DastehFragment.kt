package com.example.myshop.fragment_dasteh

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.myshop.R
import com.example.myshop.models.Dasteh
import kotlinx.android.synthetic.main.custom_dasteh.*
import kotlinx.android.synthetic.main.fragment_dasteh.*
import java.util.*
import kotlin.collections.ArrayList

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SaerchFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SaerchFragment : Fragment(),ADapterDasteh.Cliclistener {
    val dasteha=ArrayList<Dasteh>()
    lateinit var myadapter:ADapterDasteh
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dasteh, container, false)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
       dasteha()
        myadapter= ADapterDasteh(requireContext(),dasteha,this)
        rv_home.adapter=myadapter
    }
    fun dasteha() {
        val zirdasteh1=ArrayList<String>(Arrays.asList("mashin lbasshoi","oto","jaro"))
        val dasteh1=Dasteh("lavazem khanegi",zirdasteh1,false)
        dasteha.addAll(Arrays.asList(dasteh1))





    }








    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SaerchFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            SaerchFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }

    }

    override fun addclicked(position: Int) {
        dasteha.get(position).open=!dasteha.get(position).open
        myadapter.notifyItemChanged(position)

    }
}