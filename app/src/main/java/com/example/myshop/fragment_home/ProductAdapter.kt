package com.example.myshop.fragment_home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.menu.MenuView
import androidx.constraintlayout.solver.state.State
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.myshop.R
import com.example.myshop.helper.Constants
import com.example.myshop.models.Product
import kotlinx.android.synthetic.main.costum_product.view.*

class ProductAdapter(val context: Context, val products:ArrayList<Product>,val cliclistener: Cliclistener):RecyclerView.Adapter<ProductAdapter.Holder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view=LayoutInflater.from(context).inflate(R.layout.costum_product,parent,false)
        return Holder(view)

    }

    override fun getItemCount(): Int {
        return products.size

    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
       holder.itmView.tv_name_product.setText(products.get(position).name)
        holder.itmView.tv_price_product.setText(products.get(position).price.toString()+"تومان")
        Glide.with(context).load(Constants.PRODUCT_PIC_ADDRESS+products.get(position).pic).into(holder.itemView.iv_product)
        holder.itmView.setOnClickListener({
            cliclistener.itemcliced(holder.adapterPosition)
        })




    }









    class Holder(val itmView: View):RecyclerView.ViewHolder(itmView){





    }
    interface Cliclistener{
        fun itemcliced(position: Int)
    }


}