package com.example.myshop.fragment_home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.myshop.R
import com.example.myshop.api.ApiInterface
import com.example.myshop.api.retrofitmaker
import com.example.myshop.insideproduct.InsideProductFragment
import com.example.myshop.models.Product
import com.example.myshop.models.ProductsResponse
import com.example.myshop.models.RegisterResponse
import kotlinx.android.synthetic.main.fragment_home.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.create

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment(),ProductAdapter.Cliclistener {
    val products=ArrayList<Product>()
    lateinit var myadapter:ProductAdapter
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        myadapter= ProductAdapter(requireContext(),products,this)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }


    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        rv_product_main.adapter=myadapter
        pb_product.visibility=View.VISIBLE
      val api=retrofitmaker.builderetrofit().create(ApiInterface::class.java)
        api.getproducts().enqueue(object :Callback<ProductsResponse>{
            override fun onFailure(call: Call<ProductsResponse>, t: Throwable) {


            }

            override fun onResponse(
                call: Call<ProductsResponse>,
                response: Response<ProductsResponse>
            ) {
                pb_product.visibility=View.GONE
                if (response.isSuccessful){
                    products.addAll(response.body()!!.products)
                    myadapter.notifyDataSetChanged()
                }

            }

        })


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun itemcliced(position: Int) {
        val id=products[position].product_id
        childFragmentManager.beginTransaction().replace(R.id.container_home,InsideProductFragment.newInstance(id)).addToBackStack(null).commit()
    }
}